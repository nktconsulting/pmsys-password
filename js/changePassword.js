/**
 * Created by kennetvuong on 10/11/14.
 */
$(document).ready(function () {
    $('#alertBox').hide();
    //Doc ready
    console.log("Document is ready");
    $('#changePassword').click(function (e) {
        var server = $('#inputServer').val() + '/user/change_password';
        var user = $('#inputUsername').val();
        var oldPassword = $('#inputOldPassword').val();
        var newPassword = $('#inputNewPassword').val();


        //Do somepassword logic
        //Post to jquery post.
        var data = {
            'user': user,
            'password': oldPassword,
            'new_password': newPassword,
            'client': 'pmsys-password'
        };
        var headerData = {'Content-type': 'application/x-www-form-urlencoded'};

        $.ajax({
            'url': server,
            'data': data,
            'headers': headerData,
            'type': 'POST'
        })
            .done(function (data) {
                    $('#alertBox').show();
                if (data.result == 'failure') {
                    var code = data.errors[0].code;
                    var text = data.errors[0].text;
                    $('#alertBox').removeClass('alert-success').addClass('alert-danger');
                    $('#alertContent').html('<strong>'+code+'</strong> ' +  text);

                }
                else {
                    $('#alertContent').html('Successfully changed password');
                    $('#alertBox').removeClass('alert-danger').addClass('alert-success');
                    console.log(data);
                }
            })
            .fail(function (xhr, textStatus, errorThrow) {
                console.log(textStatus);
                console.log(errorThrow);
            })
        e.preventDefault();

    })
});